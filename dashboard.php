<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="container py-2">
        <div class="row bg-white p--20 shadow-md mb--20 bordered">
          <h1 class="font-weight-bold col-12 text-uppercase fs--15 text-primary mb--30">Summaries</h1>
          <div class="col-3 col-md-3">
            <div class="card-elemement">
              <h1 class="text-capitalize card-elemement__status">invoices</h1>
              <h1 class="card-elemement__count text-muted">10</h1><span onclick="window.location.href='invoices'" class="pointer card-elemement__link text-muted font-weight-medium">View all →</span>
            </div>
          </div>
          <div class="col-3 col-md-3">
            <div class="card-elemement">
              <h1 class="text-capitalize card-elemement__status">contracts</h1>
              <h1 class="card-elemement__count text-muted">7</h1><span onclick="window.location.href='contracts'" class="pointer card-elemement__link text-muted font-weight-medium">View all →</span>
            </div>
          </div>
          <div class="col-3 col-md-3">
            <div class="card-elemement">
              <h1 class="text-capitalize card-elemement__status">customers</h1>
              <h1 class="card-elemement__count text-muted">9</h1><span onclick="window.location.href='customers'" class="pointer card-elemement__link text-muted font-weight-medium">View all →</span>
            </div>
          </div>
          <div class="col-3 col-md-3">
            <div class="card-elemement">
              <h1 class="text-capitalize card-elemement__status">profiles</h1>
              <h1 class="card-elemement__count text-muted">19</h1><span onclick="window.location.href='profiles'" class="pointer card-elemement__link text-muted font-weight-medium">View all →</span>
            </div>
          </div>
        </div>
      </div>
      <!-- start:col: -->
      <div class="">

        <!-- start:portlet -->
        <div class="portlet">

          <div class="portlet-body">
            <div class="container py-3">
              <canvas id="smartyBarSimple" class="chartjs" data-chartjs-dots="false" data-chartjs-legend="true" data-chartjs-grid="true" data-chartjs-tooltip="true" data-chartjs-title="Invoice status Charts" data-chartjs-xaxes-label="Status" data-chartjs-yaxes-label="Invoices" data-chartjs-type="bar" data-chartjs-labels='["Pending", "Submitted", "Approved", "Rejected", "Paid", "Archived"]' data-chartjs-datasets='[{                                                           
                                "label":                    "Invoices",
                                "data":                     [20, 22, 24, 21, 23, 26],
                                "backgroundColor":          "rgba(139, 195, 74, 0.35)"
                            }]'></canvas>

            </div>
          </div>

        </div>
        <!-- end:portlet -->

      </div>
      <!-- end:col: -->
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
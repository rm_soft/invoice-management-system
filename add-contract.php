<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="middle-width mt--10">
        <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
          <h1 class="h4 font-weight-bold">Add Contract</h1>
        </div>
        <div class="">
          <ul class="page-action-links fs--15">
            <li><a class="active pointer">Add Contract</a></li>
            <li><a onclick="window.location.href='add-invoice'" class="  pointer">Add Invoices</a></li>
            <li><a onclick="window.location.href='add-customer'" class="  pointer">Add Customer</a></li>
          </ul>
        </div>
        <div class="middle-width__add-form rounded">
          <form class="">
            <div class="mb-3">
              <div class="mb-3 form-label-group">
                <select name="customer-select" id="customer-select" class="customer-select form-control">
                  <option value="">Select</option>
                  <option value="Customer 1">Customer 1</option>
                  <option value="Customer 2">Customer 2</option>
                </select>
                <label for="customer-select">Customer</label>
              </div>
            </div>
            <div class="mb-3 form-label-group"><input type="text" id="cname" name="cname" class="form-control form-control" value=""><label>Contract Name</label></div>
            <div class="mb-3 form-label-group"><input type="text" id="person_in_charge" name="person_in_charge" class="form-control form-control" value=""><label>Person In Charge</label></div>
            <div class="mb-3 form-label-group"><input type="text" id="dep_in_charge" name="dep_in_charge" class="form-control form-control" value=""><label>Department In Charge</label></div>
            <div class="mb-3 form-label-group">
              <div class="DayPickerInput"><input class="form-control datepicker" value="" data-today-highlight="true" data-layout-rounded="true" data-title="Datepicker" data-show-weeks="true" data-today-highlight="true" data-today-btn="true" data-clear-btn="false" data-autoclose="true" data-date-start="today"></div><label> Start Date</label>
            </div>
            <div class="mb-3 form-label-group">
              <div class="DayPickerInput"><input class="form-control datepicker" value="" data-today-highlight="true" data-layout-rounded="true" data-title="Datepicker" data-show-weeks="true" data-today-highlight="true" data-today-btn="true" data-clear-btn="false" data-autoclose="true"></div><label> End Date</label>
            </div>
            <div class=" mb-3 form-label-group"><textarea name="cDescription" id="cDescription" class="form-control" rows="4"></textarea><label>Contract Description</label></div><button id="saveEmployeeBtn" type="submit" class="btn btn-primary mt-2 w-100p">SAVE CONTRACT</button>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
<?php require_once("includes/head.php") ?>

<body>
  <div class="auth">
    <div class="auth__login-container"><img src="assets/images/logo.png" class="h-100 d-block margin-auto-0" alt="logo">
      <form action="dashboard" class=" pt--20 m--10">
        <div class="mb-3 form-label-group"><input id="userEmail" type="email" name="email" class="form-control " value=""><label>Email</label></div>
        <div class="mb-3 form-label-group"><input id="userPassword" type="password" name="password" class="form-control " value=""><label>Password</label></div><button id="loginBtn" type="submit" class="btn btn-primary mt--10 w--100p">Login</button>
      </form><a class="mt--20 d-block text-center fs--15" href="forgot-password">Forgot password?</a>
    </div>
  </div>
</body>

</html>
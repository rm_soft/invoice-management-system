<aside id="aside-main" class="aside-start aside-secondary shadow-xs font-weight-light aside-hide-xs d-flex flex-column min-height-100vh">
  <div class="d-none d-sm-block">
    <div class="clearfix d-flex justify-content-between"><a class="w-100 align-self-center navbar-brand p-3" href="dashboard"><img src="assets/images/logo.png" class="h-100" alt="logo"></a></div>
  </div>
  <div class="aside-wrapper scrollable-vertical scrollable-styled-light align-self-baseline w-100">
    <nav class="nav-deep h-100p nav-deep-sm nav-deep-light fs--15 pb-5 js-ajaxified">
      <ul class="nav flex-column">

        <li class=" nav-item"><a class="nav-link" href="dashboard"><i class="fi fi-chart-up"></i><b>Dashboard</b></a></li>
        <li class=" nav-item"><a class="nav-link" href="customers"><i class="fi fi-pie-chart"></i><b>Customers</b></a></li>
        <li class=" nav-item"><a class="nav-link" href="contracts"><i class="fi fi-folder-full"></i><b>Contracts</b></a></li>
        <li class=" nav-item"><a class="nav-link" href="invoices"><i class="fi fi-task-list"></i><b>Invoices</b></a></li>
        <li class="nav-item"><a class="nav-link" href="profiles"><i class="fi fi-users"></i><b>Profiles</b></a></li>
        <li class=" nav-item"><a class="nav-link" href="settings"><i class="fi fi-cogs"></i><b>Settings</b></a></li>
        <li class=" nav-item"><a class="nav-link" href="#"><i class="fi fi-locked"></i><b>Logout</b></a></li>
      </ul>
    </nav>
  </div>
</aside>
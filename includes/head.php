<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Invoices</title>
  <link rel="stylesheet" href="assets/css/vendor.chartjs.min.css">
  <link rel="stylesheet" href="assets/css/vendor.datepicker.min.css">
  <link rel="stylesheet" href="assets/css/vendor_bundle.min.css">
  <link rel="stylesheet" href="assets/css/globals.css">
</head>

<body>
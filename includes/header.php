<header id="header">
  <div class="container-fluid position-relative">
    <nav class="navbar navbar-expand-lg navbar-light justify-content-lg-between justify-content-md-inherit h--70">
      <div class="align-items-start"><a href="#aside-main" class="btn-sidebar-toggle h-100 d-inline-block d-lg-none justify-content-center align-items-center p-2"><span class="group-icon"><i><svg width="25" viewBox="0 0 20 20">
                <path d="M 19.9876 1.998 L -0.0108 1.998 L -0.0108 -0.0019 L 19.9876 -0.0019 L 19.9876 1.998 Z"></path>
                <path d="M 19.9876 7.9979 L -0.0108 7.9979 L -0.0108 5.9979 L 19.9876 5.9979 L 19.9876 7.9979 Z"></path>
                <path d="M 19.9876 13.9977 L -0.0108 13.9977 L -0.0108 11.9978 L 19.9876 11.9978 L 19.9876 13.9977 Z"></path>
                <path d="M 19.9876 19.9976 L -0.0108 19.9976 L -0.0108 17.9976 L 19.9876 17.9976 L 19.9876 19.9976 Z"></path>
              </svg></i><i><svg width="25" viewBox="0 0 47.971 47.971">
                <path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path>
              </svg></i></span></a><a class="navbar-brand d-inline-block d-lg-none" href="#"><img src="/images/logo.png" alt="..."></a>
        <form action="search-results">
          <div class="search_container"><input type="text" placeholder="Search invoices, contracts, customer keywords" class="header_search" value=""><button class="btn searchBtn btn-sm"><i class="fi fi-search"></i></button></div>
        </form>
      </div>
      <div class="collapse navbar-collapse justify-content-end" id="navbarMainNav">
        <div class="navbar-xs d-none"><button class="navbar-toggler pt-0" type="button" data-toggle="collapse" data-target="#navbarMainNav" aria-controls="navbarMainNav" aria-expanded="false" aria-label="Toggle navigation"><svg width="20" viewBox="0 0 20 20">
              <path d="M 20.7895 0.977 L 19.3752 -0.4364 L 10.081 8.8522 L 0.7869 -0.4364 L -0.6274 0.977 L 8.6668 10.2656 L -0.6274 19.5542 L 0.7869 20.9676 L 10.081 11.679 L 19.3752 20.9676 L 20.7895 19.5542 L 11.4953 10.2656 L 20.7895 0.977 Z"></path>
            </svg></button><a class="navbar-brand px-4 w-auto" href="index.html"><img src="images/logo.png" alt="..."></a></div>
        <ul class="list-inline list-unstyled mb-0 d-flex align-items-end">
          <li class="list-inline-item ml--6 mr--6 dropdown"><a id="dropdownNotificationOptions" class="btn btn-sm rounded-circle btn-light dropdown-toggle" href="notifications"><span class="group-icon"><i class="fi fi-bell-full"></i></span></a></li>
          <li class="list-inline-item ml--6 mr--6 dropdown">
            <div class="d-flex justify-content-start align-items-center ">
              <div class="profile-image "><img src="assets/images/user-avatar_nch70m.png" class="profile-photo" alt="progile-photo"></div><span class="userName">Accountant</span>
            </div>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</header>
<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill pt--20">
      <div class="container">
        <div class="row pt--10">
          <div class="col-lg-8 rounded bg-white p--40">
            <h1 class="fs--25 text-primary mb--40 font-weight-bold d-space"><span>INVOICE</span><span>#45678</span></h1>
            <div class="invoice-details mb--30">
              <div class="d-space list-item"><span>Received Date</span><span>2020-09-17</span></div>
              <div class="d-space list-item"><span>Payment Deadline</span><span>2020-11-01</span></div>
              <div class="d-space list-item"><span>Status</span><span class="paid">pending</span></div>
            </div>
            <h1 class="fs--14 mb--20 font-weight-bold d-space"><span>CONTRACT</span></h1>
            <div class="invoice-details mb--30">
              <div class="d-space list-item"><span>Name</span><span>Amudala Contract</span></div>
              <div class="d-space list-item"><span>Description</span><span> Amudala Contract Description</span></div>
            </div>
            <h1 class="fs--15 mb--20 font-weight-bold d-space"><span>OWNER | CUSTOMER</span></h1>
            <div class="invoice-details mb--30">
              <div class="d-space list-item"><span>Name</span><span>Amudala</span></div>
              <div class="d-space list-item"><span>Address (Location)</span><span>Kigali</span></div>
              <div class="d-space list-item"><span>Email </span><span>amudala@gmail.com</span></div>
              <div class="d-space list-item"><span>Tel (Offfice)</span><span>3456543</span></div>
              <div class="d-space list-item"><span>Mobile</span><span>0782061111</span></div>
              <div class="d-space list-item"><span>Contact</span><span>+25078803921</span></div>
              <div class="d-space list-item"><span>TIN Number</span><span>None</span></div>
              <div class="d-space list-item"><span>Representative</span><span>None</span></div>
              <div class="d-space list-item"><span>Registered Date </span><span>None</span></div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="box-shadow bg-white mb--15 rounded">
              <h1 class="fs--13 p--15 mb--0 border-bottom font-weight-medium d-space"><span>DOCUMENTS</span></h1>
              <div class="p--20">
                <div class="invoice-details no-border-last-element">No Files...</div>
              </div>
            </div>
            <div class="box-shadow bg-white mb--15 rounded">
              <h1 class="fs--13 p--15 mb--0 border-bottom font-weight-medium d-space"><span>CHANGE STATUS</span></h1>
              <div class="p--20">
                <div class=" mb-3 form-label-group"><select name="status" id="status" class="form-control">
                    <option value=""> </option>
                    <option value="Approved">Approved</option>
                    <option value="Rejected">Rejected</option>
                    <option value="Paid">Paid</option>
                  </select><label>Status</label></div>
              </div>
            </div>
            <div class="box-shadow bg-white mb--15 rounded">
              <h1 class="fs--13 p--15 mb--0 border-bottom font-weight-medium d-space"><span>COMMENTS</span></h1>
              <div class="invoice-comment">
                <div>
                  <div class="add-comment-field p--20">
                    <div class=" mb-3 form-label-group"><textarea name="invoiceComment" id="invoiceComment" class="form-control" rows="2"></textarea><label>Type Comment</label></div><button id="saveCommentBtn" type="submit" class="btn btn-sm btn-primary">Add</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
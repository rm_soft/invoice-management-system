<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="w-80-desk mt--10">
        <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
          <h1 class="h4 font-weight-bold">Add Customer</h1>
        </div>
        <div class="middle-width__add-form rounded mt--30">
          <div class="md-2"></div>
          <form class="row">
            <div class="col-md-6 mb-3 mb-3 form-label-group">
              <select name="customerType" id="customerType" class="form-control">
                <option value="">Select</option>
                <option value="Individual">Individual</option>
                <option value="Company">Company</option>
              </select><label>Customer Type</label>
            </div>
            <div class="col-md-6 mb-3 d-none customerName form-label-group">
              <input id="customerName" type="text" name="customerName" class="form-control" value="">
              <label for="customerName">Customer name</label>
            </div>

            <div class="col-md-6 mb-3 form-label-group">
              <input type="text" id="companyTIN" name="companyTIN" class="form-control form-control" value="">
              <label>Tin Number</label>
            </div>
            <div class="appendInput row pl--15 pr--0" style="width: 100%;"></div>
            <div class="col-md-6 mb-3 form-label-group"><input type="number" id="AccountNumber" name="accountNumber" class="form-control form-control" value=""><label>Account Number</label></div>
            <div class="col-md-6 mb-3 form-label-group"><input type="text" id="addressLocation" name="addressLocation" class="form-control form-control" value=""><label>Address Location</label></div>
            <div class="col-md-6 mb-3 form-label-group"><input type="email" id="email" name="email" class="form-control form-control-md" value=""><label>Email</label></div>
            <div class="col-md-6 mb-3 form-label-group"><input type="text" id="telOffice" name="telOffice" class="form-control form-control" value=""><label>Tel (Office)</label></div>
            <div class="col-md-6 mb-3 form-label-group"><input type="text" id="Mobile" name="mobile" class="form-control form-control" value=""><label>Mobile</label></div>
            <div class="col-md-6 mb-3 form-label-group"><input type="text" id="contact" name="contact" class="form-control form-control" value=""><label>Contact</label></div><button id="saveEmployeeBtn" type="submit" class="btn btn-secondary font-weight-medium ml--15 mr--15 mt--30 w-100p">SAVE CUSTOMER</button>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
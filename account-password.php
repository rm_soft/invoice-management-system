<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="middle-width mt--30">
        <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
          <h1 class="h4 font-weight-bold">Change Password</h1>
        </div>
        <div class="middle-width__add-form rounded">
          <form>
            <div class="mb-3 form-label-group"><input id="currentPassword" type="password" name="currentPassword" class="form-control form-control" value=""><label>Current Password</label></div>
            <div class="mb-3 form-label-group"><input id="password" type="password" name="password" class="form-control form-control" value=""><label>Password</label></div>
            <div class="mb-3 form-label-group"><input id="confirm_password" type="password" name="confirm_password" class="form-control form-control" value=""><label>Confirm Password</label></div><button id="saveEmployeeBtn" type="submit" class="btn btn-primary font-weight-medium mt-2 w-100p">UPDATE PASSWORD</button>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
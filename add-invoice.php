<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill pt--40">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
              <h1 class="h4 font-weight-bold">Add Invoice</h1>
            </div>
          </div>
          <div class="col-lg-4"><label class="d-block fs--13 mt---5">Search and select contract if already exists</label>
            <div class="min-width-300 d-block css-2b097c-container">
              <select name="customer-select" id="customer-select" class="customer-select form-control">
                <option value="">Select</option>
                <option value="Customer 1">Customer 1</option>
                <option value="Customer 2">Customer 2</option>
              </select>
            </div>
          </div>
        </div>
        <div class="">
          <ul class="page-action-links fs--15">
            <li><a class="active pointer">Add Invoice</a></li>
            <li><a class="  pointer">Add Contract</a></li>
            <li><a class="  pointer">Add Customer</a></li>
          </ul>
        </div>
        <form class="row pt--20">
          <div class="col-lg-8"></div>
          <div class="col-lg-8">
            <div class="mb-3 form-label-group"><input id="invoiceNumber" type="text" name="invoiceNumber" class="form-control form-control" value=""><label>Invoice Number</label></div>
            <div class="mb-3 form-label-group"><input id="contractID" type="text" name="contractID" class="form-control form-control" readonly="" value=""><label>Contract ID / Number</label></div>
            <div class="mb-3 form-label-group"><input id="owner" type="text" name="owner" class="form-control form-control" readonly="" value=""><label>Owner / Customer</label></div><button id="saveInvoiceBtn" type="submit" class="btn btn-primary mt--30 w-100p">Add Invoice</button>
          </div>
          <div class="col-lg-4">
            <div class="box-shadow rounded p--10 scrollable-vertical  align-self-baseline w-100 h-300"></div>
          </div>
        </form>
      </div>
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
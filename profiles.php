<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
        <h1 class="h4 font-weight-bold">System Users</h1>
        <button id="addUserBtn" onclick="window.location.href='add-user'" class="undefined btn btn-sm btn-pill shadow-md d-flex align-items-center justify-conitent-start">
          <i class="fi fs--18 fi-user-plus"></i><span>Add User</span></button>
      </div>
      <div class="">
        <ul class="page-action-links fs--15">
          <li><a class="active pointer">Users List</a></li>
          <li><a class="  pointer">Accountant</a></li>
          <li><a class="  pointer">Secretary</a></li>
          <li><a class="  pointer">DM</a></li>
        </ul>
      </div>
      <div class="container py-2">
        <div class="row mt--20 fs--14 font-weight-medium pb--10">
          <div class="col-lg-4">Names</div>
          <div class="col-lg-2">Gender</div>
          <div class="col-lg-2">Position</div>
          <div class="col-lg-3">Office</div>
          <div class="col-lg-1">Status</div>
        </div>
        <div class="row">
          <div class="col-lg-12 bg-white list mb--10 p--15">
            <div class="row">
              <div class="col-lg-4 d-flex justify-content-start align-items-center"><span class="placeholder br-50  bg-success">A</span>
                <div><span class="font-weight-medium">Admin</span><span class="fs--13 d-block mt---4">admin@gmail.com</span></div>
              </div>
              <div class="col-lg-2">Male</div>
              <div class="col-lg-2">Secretary</div>
              <div class="col-lg-3">Secretary Office</div>
              <div class="col-lg-1">Active</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require_once("includes/footer.php") ?>
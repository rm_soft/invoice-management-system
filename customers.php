<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
        <h1 class="h4 font-weight-bold">Customers</h1>
        <button onclick="window.location.href='add-customer'" class="btn addUserBtn btn-pill btn-success btn-sm">
          <i class="fi fs--18 fi-users"></i>
          <span>Add Customer</span>
        </button>
      </div>
      <div class="">
        <ul class="page-action-links fs--15">
          <li><a class="active pointer">All Customer</a></li>
          <li><a class="  pointer">Individual</a></li>
          <li><a class="  pointer">Company</a></li>
          <li><a class="  pointer">Active</a></li>
          <li><a class="  pointer">Inactive</a></li>
        </ul>
      </div>
      <div class="container py-2">
        <div class="row mt--20 fs--14 font-weight-medium pb--10">
          <div class="col-lg-3">Customer Name / Company name</div>
          <div class="col-lg-2">Type / Client No.</div>
          <div class="col-lg-2">Mobile / Tel</div>
          <div class="col-lg-3">Account No. / Address</div>
          <div class="col-lg-1">Status</div>
          <div class="col-lg-1">More</div>
        </div>
        <div class="row">
          <div class="col-lg-12 bg-white list mb--10 p--15">
            <div class="row">
              <div class="col-lg-3 d-flex justify-content-start align-items-center"><span class="placeholder rounded bg-secondary">N</span>
                <div><span class="font-weight-medium">Naasoni</span><span class="fs--13 d-block"><span class="font-weight-medium"></span>Naason</span></div>
              </div>
              <div class="col-lg-2">
                <div><span class="font-weight-medium">Company</span><span class="fs--13 d-block "><span class="font-weight-medium text-indigo">#0009</span></span></div>
              </div>
              <div class="col-lg-2 font-weight-medium">
                <div><span class="font-weight-medium fs--15">0782062364</span><span class="fs--13 d-block "><span class="font-weight-medium">92384</span></span></div>
              </div>
              <div class="col-lg-3 font-weight-medium">
                <div><span class="font-weight-medium fs--15">23456543456543</span><span class="fs--13 d-block "><span class="font-weight-medium"> Kigali</span></span></div>
              </div>
              <div class="text-success  font-weight-medium col-lg-1 fs--15">active</div>
              <div class="col-lg-1"><a class="fs--13" href="customer-details">More →</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require_once("includes/footer.php") ?>
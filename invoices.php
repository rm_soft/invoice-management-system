<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
        <h1 class="h4 font-weight-bold">Customer Invoices</h1>
        <button onclick="window.location.href='add-invoice'" class="btn addUserBtn btn-pill btn-secondary btn-sm">
          <i class="fi fs--18 fi-task-list"></i>
          <span>Add Invoice</span>
        </button>
      </div>
      <div class="">
        <ul class="page-action-links fs--15">
          <li><a class="active pointer">All Invoices</a></li>
          <li><a class="  pointer">Pending</a></li>
          <li><a class="  pointer">Submitted</a></li>
          <li><a class="  pointer">Rejected</a></li>
          <li><a class="  pointer">Paid</a></li>
          <li><a class="  pointer">Archived</a></li>
          <li><a class="  pointer">Approved</a></li>
        </ul>
      </div>
      <div class="container py-2">
        <div class="row mt--20 fs--14 font-weight-medium pb--10">
          <div class="col-lg-4">Customer / Invoice Number</div>
          <div class="col-lg-3">Contract ID / Customer ID</div>
          <div class="col-lg-2">Received / Payment Date</div>
          <div class="col-lg-2">Status</div>
          <div class="col-lg-1">More</div>
        </div>
        <div class="row">
          <div class="col-lg-12 bg-white list mb--10 p--15">
            <div class="row">
              <div class="col-lg-4 d-flex justify-content-start align-items-center"><span class="placeholder rounded bg-success">G</span>
                <div><span class="font-weight-medium">Galu Djally</span><span class="fs--13 d-block ">#19629865</span></div>
              </div>
              <div class="col-lg-3">
                <div><span class="font-weight-medium">#0001</span><span class="fs--13 d-block "><span class="text-success">#0001</span></span></div>
              </div>
              <div class="col-lg-2 font-weight-medium">
                <div><span class="font-weight-medium fs--15">2020-09-23</span><span class="fs--13 d-block ">2020-11-07</span></div>
              </div>
              <div class="submitted col-lg-2 ">submitted</div>
              <div class="col-lg-1"><a class="fs--13" href="invoice-details">More →</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
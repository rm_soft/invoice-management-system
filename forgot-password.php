<?php require_once("includes/head.php") ?>

<body>
  <div class="auth">
    <div class="auth__login-container">

      <img src="assets/images/logo.png" class="h-100 d-block margin-auto-0" alt="logo">
      <form action="#" class=" pt--20 m--10">
        <div class="mb-3 form-label-group"><input id="userEmail" type="email" name="userEmail" class="form-control " value="amudala41@gmail.com"><label>Email</label></div><button id="forgotPassBtn" type="submit" class="btn btn-primary mt--10 w--100p">Recover Password</button>
      </form><a class="d-block text-center" href="index">← Back to login</a>
    </div>
  </div>
</body>

</html>
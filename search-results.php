<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
        <h1 class="h4 font-weight-bold">Search Results</h1>
      </div>
      <div class="container py-2">
        <div class="row noMessage mt--20 fs--14 font-weight-medium pb--10">
          <div class="img-container"><img src="assets/images/no_results.png" alt="">
            <h3 class="mb--50 pt--25 fs--20">Oops!! no results found.</h3>
          </div>
        </div>

        <div class="row mt--20 fs--14 font-weight-medium pb--10">
          <div>
            <h5>Contract search results</h5>
          </div><a onclick="window.location.href='contract-details'" class="search_result"> <i class="fi fi-arrow-right fs--10 mr--10"></i> Contract found matches contract: " Naasoni "</a>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
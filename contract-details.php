<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill pt--40">
      <div class="container">
        <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
          <h1 class="h4 font-weight-bold">Contract</h1>
        </div>
        <div class="row pt--10">
          <div class="col-lg-8">
            <div class="bg-white rounded p--40 bg-white">
              <div class="invoice-details mb--30">
                <div class="d-space list-item"><span>Contract Name</span><span>Amudala Contract</span></div>
                <div class="d-space list-item"><span>Contract Date </span><span>2020-09-10</span></div>
                <div class="d-space list-item"><span>Customer Name</span><span>Amudala</span></div>
                <div class="d-space list-item"><span>Customer Email</span><span>amudala@gmail.com</span></div>
                <div class="d-space list-item"><span>Customer Contact </span><span>+25078803921</span></div>
                <div class="d-space list-item"><span>Customer Type</span><span>Individual</span></div>
                <div class="d-space list-item"><span>Customer TIN Number</span><span>876534</span></div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="box-shadow bg-white mb--15 rounded">
              <h1 class="fs--13 p--15 mb--0 border-bottom font-weight-medium d-space"><span>CONTRACT DOCS</span></h1>
              <div class="invoice-comment">
                <div class="position-relative scrollable-vertical  align-self-baseline w-100">
                  <div class="list-item">
                    <div class="p--20">No contract doc found</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
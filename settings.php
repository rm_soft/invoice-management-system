<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
        <h1 class="h4 font-weight-bold">Settings</h1>
      </div>
      <div class="">
        <ul class="page-action-links fs--15">
          <li><a class="active pointer">My Profile</a></li>
          <li><a onclick="window.location.href='account-password'" class="pointer">Change Password</a></li>
        </ul>
      </div>
      <div class="container py-2">
        <div class="row mt--20 fs--15 font-weight-medium pb--10">
          <div class="col-md-12 pl--0">
            <p class="bg-white d-flex justify-content-start align-items p--15 rounded mb--5"><span class="text-capitalize min-width-90">Names</span><span class="text-capitalize font-weight-normal">null null</span></p>
          </div>
          <div class="col-md-12 pl--0">
            <p class="bg-white d-flex justify-content-start align-items p--15 rounded mb--5"><span class="text-capitalize min-width-90">Email</span><span class="font-weight-normal">juliushirwa@gmail.com</span></p>
          </div>
          <div class="col-md-12 pl--0">
            <p class="bg-white d-flex justify-content-start align-items p--15 rounded mb--5"><span class="text-capitalize min-width-90">Gender</span><span class="text-capitalize font-weight-normal">null</span></p>
          </div>
          <div class="col-md-12 pl--0">
            <p class="bg-white d-flex justify-content-start align-items p--15 rounded mb--5"><span class="text-capitalize min-width-90">Office</span><span class="text-capitalize font-weight-normal">null</span></p>
          </div>
          <div class="col-md-12 pl--0">
            <p class="bg-white d-flex justify-content-start align-items p--15 rounded mb--5"><span class="text-capitalize min-width-90">Status</span><span class="text-capitalize font-weight-normal">null</span></p>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
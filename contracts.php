<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
        <h1 class="h4 font-weight-bold">Contracts</h1>
        <button onclick="window.location.href='add-contract'" class="btn addUserBtn btn-pill btn-primary btn-sm">
          <i class="fi fs--18 fi-layers-middle"></i>
          <span>Add Contract</span>
        </button>
      </div>
      <div class="">
        <ul class="page-action-links fs--15">
          <li><a class="active pointer">All Contracts</a></li>
          <li><a class="  pointer">Valid</a></li>
          <li><a class="  pointer">Invalid</a></li>
        </ul>
      </div>
      <div class="container py-2">
        <div class="row mt--20 fs--14 font-weight-medium pb--10">
          <div class="col-lg-4"><b>Name</b></div>
          <div class="col-lg-3"><b>Description</b></div>
          <div class="col-lg-2"><b>Started At / End At</b></div>
          <div class="col-lg-2"><b>Person / Department In Charge</b></div>
          <div class="col-lg-1"><b>More</b></div>
        </div>
        <div class="row">
          <div class="col-lg-12 bg-white fs--15 list mb--10 p--15">
            <div class="row">
              <div class="col-lg-4 d-flex justify-content-start align-items-center"><span class="placeholder br-50  bg-purple">N</span>
                <div><span class="font-weight-medium">Naasoni</span></div>
              </div>
              <div class="col-lg-3">Naasoni new contract</div>
              <div class="col-lg-2"><span class="font-weight-medium">2020-10-21</span><span class="fs--14 d-block "><span class="font-weight-medium text-warning">2020-10-31</span></span></div>
              <div class="col-lg-2"><span class="font-weight-medium">Kagabo Faustin</span><span class="fs--14 d-block "><span class="font-weight-medium text-warning">Finance</span></span></div>
              <div class="col-lg-1 text-indigo fs--14"><a class="fs--13" href="contract-details">More →</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once("includes/footer.php") ?>
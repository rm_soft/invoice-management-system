$("#customerType").change(function () {
  let customerName = `<div class="col-md-12 mb-3 pr--0 form-label-group"><input id="customerName" type="text" name="customerName" class="form-control"><label class="d-block pl--30">Customer name</label></div>`;

  let companyDetails = `<div class="col-md-6 mb-3 form-label-group"><input id="customerName" type="text" name="customerName" class="form-control form-control"><label>Company Name</label></div><div class="col-md-6 mb-3 pr--0 form-label-group"><input id="companyRepr" type="text" name="companyRepr" class="form-control form-control"><label>Company Representative</label></div>
  <div class="col-md-12 pr--0"><div class="mb-3 form-label-group"><input id="datepicker" class="form-control datepicker" ><label> Registered Date</label></div></div>`;

 if ($(this).val() === 'Individual') {
    $('.appendInput').html(customerName)
  } else if ($(this).val() === 'Company') {
    $('.appendInput').html(companyDetails)
  } else {
    $('.appendInput').html('')
  }
})
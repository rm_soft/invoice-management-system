<?php require_once("includes/head.php") ?>
<div id="wrapper" class="d-flex align-items-stretch flex-column">
  <!-- HEADER -->
  <?php require_once("includes/header.php") ?>

  <div id="wrapper_content" class="d-flex flex-fill">

    <!-- ASIDE -->
    <?php require_once("includes/aside.php") ?>

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
      <div class="middle-width">
        <div class="page-title mb--20 bg-transparent b-0 d-flex justify-content-between align-items-center">
          <h1 class="h4 font-weight-bold">Add Profile</h1>
        </div>
        <div class="middle-width__add-form rounded">
          <form>
            <div class="mb-3 form-label-group"><input id="fname" type="text" name="fname" class="form-control form-control-md" value=""><label>First Name</label></div>
            <div class="mb-3 form-label-group"><input id="lname" type="text" name="lname" class="form-control form-control-md" value=""><label>Last Name</label></div>
            <div class=" mb-3 form-label-group"><select name="gender" id="gender" class="form-control">
                <option value=""> </option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select><label>Gender</label>
            </div>
            <div class=" mb-3 form-label-group"><select name="position" id="position" class="form-control">
                <option value=""> </option>
                <option value="DAFF">DAFF</option>
                <option value="Accountant">Accountant</option>
                <option value="Secretary">Secretary</option>
                <option value="DM">DM</option>
              </select><label>Position</label>
            </div>
            <div class=" mb-3 form-label-group">
              <select name="office" id="office" class="form-control">
                <option value=""> </option>
                <option value="Secretary Office">Secretary Office</option>
                <option value="DM Office">DM Office</option>
              </select>
              <label>Office</label>
            </div>
            <div class="mb-3 form-label-group">
              <input id="username" type="text" name="username" class="form-control form-control" value="">
              <label>Username</label>
            </div>
            <div class="mb-3 form-label-group"><input id="email" type="email" name="email" class="form-control form-control" value=""><label>Email</label></div>
            <div class="mb-3 form-label-group"><input id="password" type="password" name="password" class="form-control form-control" value=""><label>Password</label></div><button id="saveEmployeeBtn" type="submit" class="btn btn-primary mt-2 w-100p">Save Profile</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require_once("includes/footer.php") ?>